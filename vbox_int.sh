pacman -S virtualbox virtualbox-host-modules-arch virtualbox-guest-iso virtualbox-ext-vnc net-tools

#Должен быть такой конфиг вбоха
tee /etc/modules-load.d/virtualbox.conf <<EOF
	vboxdrv
	vboxnetadp	#Создание интерфейса в глобальных настройках Вбох
	vboxnetflt	#Даёт запуск виртуали с использованием интерфейса vboxnetadr
	vboxpci
EOF

#Подгрузка конфига
modprobe vboxdrv

#Обновление модулей ядра Вбох
vboxreload

#Настройки в GUI vbox:
	#Ctrl+H > Менеджер сетей хоста > Создать > Применить
	#Далее: локальная сеть на вирт-тачках будет работать :) 
